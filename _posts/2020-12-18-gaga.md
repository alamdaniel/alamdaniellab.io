---
layout: post  
title: "TopTen: Lady Gaga"  
date: 2020-12-18
categories: podcast
image: images/10.png
podcast_link: https://archive.org/download/ladygagatopten/31.%20Lady%20Gaga2
tags: [audio, podcast, cultura, Cataclismos]  
comments: true 
---
Es un ícono de la cultura pop actual, ¡Sin ninguna duda! Su estilo, su voz, sus vestuarios y mucho más hacen de Lady Gaga una artista completa. Sus millones de fans en el mundo y sus canciones nos afirman que es una estrella capaz de romper esquemas y trascender en la industria de la música. Y en este episodio, escucharás sus 10 mejores canciones. 

<audio controls>
  <source src="https://archive.org/download/ladygagatopten/31.%20Lady%20Gaga2.ogg" type="audio/ogg">
  <source src="https://archive.org/download/ladygagatopten/31.%20Lady%20Gaga2.mp3" type="audio/mpeg">
</audio>

![#Gaga](http://alamdaniel.gitlab.io/images/10.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Instagram: <https://www.instagram.com/alam.dnl/>
+ <https://www.instagram.com/cataclismos_podcast/>
+ Correo: <cataclismospodcast@outlook.com>
