---
layout: post  
title: "Quédate en Casa HDTPM"  
date: 2020-11-13
categories: podcast
image: images/06.png
podcast_link: https://archive.org/download/review-coronavirus/27.%20REFLEXION%20DE%20CUARENTENA
tags: [audio, podcast, cultura, Cataclismos]  
comments: true 
---
Agg 2020 (Léase con voz del narrador de Bob Esponja) el año del COVID-19, en este episodio con música cute de fondo hago un pequeño review que pretendía ser motivante amigos pero, me acordé de esa gente a la que le vale nepe la cuarentena (Covidiotas) y terminó siendo un review agridulce con un poco de Dross, Cardi B y Canelita (El Conejito Chi Cheñol). 
No olvides quedarte en casita HDTPM (Hoy Dios Te Premiará Mucho).

<audio controls>
  <source src="https://archive.org/download/review-coronavirus/27.%20REFLEXION%20DE%20CUARENTENA.ogg" type="audio/ogg">
  <source src="https://archive.org/download/review-coronavirus/27.%20REFLEXION%20DE%20CUARENTENA.mp3" type="audio/mpeg">
</audio>

![#Coronavirus](http://alamdaniel.gitlab.io/images/06.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Instagram: <https://www.instagram.com/alam.dnl/>
+ <https://www.instagram.com/cataclismos_podcast/>
+ Correo: <cataclismospodcast@outlook.com>
