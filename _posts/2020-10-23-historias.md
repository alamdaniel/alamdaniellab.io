---
layout: post  
title: "Historias de Terror (Parte 1)"  
date: 2020-10-23
categories: podcast
image: images/03.png
podcast_link: https://archive.org/download/historiasdeterror_2020/24.Historiasdeterror
tags: [audio, podcast, cultura, Cataclismos]  
comments: true 
---
Chaneques, La Llorona, El Nahual y Fantasmas, estas son las criaturas macabras que han aterrado en diversas situaciones a Víctor, Brandon, Jorge Luis y Alejandro. En este episodio, ellos nos relatan con voz propia sus historias con estos sucesos paranormales, 4 testimonios que te dejarán mucho que pensar.

<audio controls>
  <source src="https://archive.org/download/historiasdeterror_2020/24.Historiasdeterror.ogg" type="audio/ogg">
  <source src="https://archive.org/download/historiasdeterror_2020/24.Historiasdeterror.mp3" type="audio/mpeg">
</audio>

![#Terror](http://alamdaniel.gitlab.io/images/03.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Instagram: <https://www.instagram.com/alam.dnl/>
+ <https://www.instagram.com/cataclismos_podcast/>
+ Correo: <cataclismospodcast@outlook.com>
