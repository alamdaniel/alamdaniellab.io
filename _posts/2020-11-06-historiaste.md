---
layout: post  
title: "Historias de Terror (Parte 2)"  
date: 2020-11-06
categories: podcast
image: images/05.png
podcast_link: https://archive.org/download/historias-terror2/26.HistoriasTerror2
tags: [audio, podcast, cultura, Cataclismos]  
comments: true 
---
Segunda entrega de historias de terror por 3 personas que han tenido experiencias paranormales y que te dejarán pensando esta noche antes de ir a la cama, cuídate, y no mires debajo de tu cama en la madrugada.

<audio controls>
  <source src="https://archive.org/download/historias-terror2/26.HistoriasTerror2.ogg" type="audio/ogg">
  <source src="https://archive.org/download/historias-terror2/26.HistoriasTerror2.mp3" type="audio/mpeg">
</audio>

![#Terror](http://alamdaniel.gitlab.io/images/05.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Instagram: <https://www.instagram.com/alam.dnl/>
+ <https://www.instagram.com/cataclismos_podcast/>
+ Correo: <cataclismospodcast@outlook.com>
